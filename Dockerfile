FROM golang:1.11-alpine as test

ARG PROJECT_PATH
WORKDIR /go/src/${PROJECT_PATH}  
COPY .  .
RUN ls -la && pwd && apk add --update git \
    go get &&\
    CGO_ENABLED=0 go test ./.. || CGO_ENABLED=0 go test

FROM golang:1.11-alpine as builder
ARG PROJECT_PATH
WORKDIR /go/src/${PROJECT_PATH}  
COPY .  .
RUN go get &&\
    CGO_ENABLED=0 GOOS=linux go build -o main .

FROM alpine:latest  
ARG PROJECT_PATH
RUN apk --no-cache add ca-certificates
WORKDIR /app/
COPY --from=builder /go/src/${PROJECT_PATH}/main .
CMD ["./main"]  