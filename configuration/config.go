package configuration

import (
	"fmt"
	"log"

	"gitlab.com/tribock/ix-vault/util/yaml"
)

// Foo bar
func Foo() {
	fmt.Printf("foo")
	bar()
}

func bar() {
	fmt.Println("bar")
}

type Config struct {
	Vault VaultConfig
}

type VaultConfig struct {
	VaultAdress string
	VaultToken  string
}

// ReadConfig from Config folder
func ReadConfig() *Config {
	config := &Config{}
	if err := yaml.ReadConfig(config,
		"./configuration/config.yaml",
		"./configuration/config_$ENV.yaml"); err != nil {
		log.Println(err)
	}
	return config
}
