module gitlab.com/tribock/ix-vault

go 1.12

require (
	github.com/ghodss/yaml v1.0.0
	github.com/hashicorp/vault/api v1.0.2
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
