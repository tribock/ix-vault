package main

import (
	"log"

	"gitlab.com/tribock/ix-vault/configuration"
	"gitlab.com/tribock/ix-vault/vault"
)

var VClient *vault.VaultClient // global variable

func main() {
	config := configuration.ReadConfig()
	log.Println(config.Vault.VaultAdress, config.Vault.VaultToken)
	VClient, err := vault.GetVaultClient(config.Vault.VaultAdress, config.Vault.VaultToken)
	if err != nil {
		log.Println(err)
	}
	c := VClient.Logical()

	secret, err := c.Write("kv/reni",
		map[string]interface{}{
			"name":     "reni",
			"username": "spm",
			"password": "1346",
		})
	if err != nil {
		log.Println(err)
	}
	log.Println(secret)
	vault.PKI(VClient)

}
