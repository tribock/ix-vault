package util

import (
	"io/ioutil"
	"log"
	"os"
)

type ConfigReader interface {
	Unmarshal(data []byte, o interface{}) error
}

type ValidateableConfiguration interface {
	Validate() error
}

type ConfigReaderFunc func(data []byte, o interface{}) error

func (c ConfigReaderFunc) Unmarshal(data []byte, o interface{}) error {
	return c(data, o)
}

func readConfigFile(configReader ConfigReader, configFile string, obj interface{}) error {
	configFile = os.ExpandEnv(configFile)

	if _, err := os.Stat(configFile); err != nil {
		log.Println(err)
		return nil
	}

	configStr, err := ioutil.ReadFile(configFile) //nolint: gosec
	if err != nil {
		log.Println(err)
		return err
	}

	configStr = []byte(os.ExpandEnv(string(configStr)))

	if err := configReader.Unmarshal(configStr, obj); err != nil {
		log.Println(err)
		return err
	}

	return nil
}

// ReadConfig deserializes each configfile to the target obj using the configReader
// env vars are replaced in the file path
func ReadConfig(configReader ConfigReader, obj interface{}, configFiles ...string) error {
	for _, cf := range configFiles {
		if err := readConfigFile(configReader, cf, obj); err != nil {
			return err
		}
	}

	if validateable, ok := obj.(ValidateableConfiguration); ok {
		if err := validateable.Validate(); err != nil {
			return err
		}
	}

	return nil
}
