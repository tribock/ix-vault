package yaml

import (
	"github.com/ghodss/yaml"
	"gitlab.com/tribock/ix-vault/util"
)

var ConfigReader = util.ConfigReaderFunc(yamlUnmarshalNoOpts)

func yamlUnmarshalNoOpts(y []byte, o interface{}) error {
	return yaml.Unmarshal(y, o)
}

func ReadConfig(obj interface{}, configFiles ...string) error {
	return util.ReadConfig(ConfigReader, obj, configFiles...)
}
