package vault

import (
	"github.com/hashicorp/vault/api"
)

type VaultClient struct {
	*api.Client
}

func (c *VaultClient) createPKIEngine(path string) error {
	return c.Sys().Mount(path, &api.MountInput{
		Type: "pki",
		Config: api.MountConfigInput{
			DefaultLeaseTTL: "16h",
			MaxLeaseTTL:     "32h",
		},
	})
}
