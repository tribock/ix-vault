package vault

import (
	"log"
)

func PKI(client *VaultClient) {

	client.createPKIEngine("pki")

	resp, err := client.Logical().Write("pki/root/generate/internal", map[string]interface{}{
		"common_name": "myvault.com",
	})
	if err != nil {
		log.Fatal(err)
	}
	if resp == nil {
		log.Fatal("expected ca info")
	}

	// Create a role which does require CN (default)
	_, err = client.Logical().Write("pki/roles/example", map[string]interface{}{
		"allowed_domains":    "foobar.com,zipzap.com,abc.com,xyz.com",
		"allow_bare_domains": true,
		"allow_subdomains":   true,
		"max_ttl":            "2h",
	})

	// Issue a cert with require_cn set to true and with common name supplied.
	// It should succeed.
	resp, err = client.Logical().Write("pki/issue/example", map[string]interface{}{
		"common_name": "foobar.com",
	})
	if err != nil {
		log.Fatal(err)
	}

	// Issue a cert with require_cn set to true and with out supplying the
	// common name. It should error out.
	resp, err = client.Logical().Write("pki/issue/example", map[string]interface{}{})
	if err == nil {
		log.Fatalf("expected an error due to missing common_name")
	}

	// Modify the role to make the common name optional
	_, err = client.Logical().Write("pki/roles/example", map[string]interface{}{
		"allowed_domains":    "foobar.com,zipzap.com,abc.com,xyz.com",
		"allow_bare_domains": true,
		"allow_subdomains":   true,
		"max_ttl":            "2h",
		"require_cn":         false,
	})

	// Issue a cert with require_cn set to false and without supplying the
	// common name. It should succeed.
	resp, err = client.Logical().Write("pki/issue/example", map[string]interface{}{})
	if err != nil {
		log.Fatal(err)
	}

	if resp.Data["certificate"] == "" {
		log.Fatalf("expected a cert to be generated")
	}

	// Issue a cert with require_cn set to false and with a common name. It
	// should succeed.
	resp, err = client.Logical().Write("pki/issue/example", map[string]interface{}{})
	if err != nil {
		log.Fatal(err)
	}

	if resp.Data["certificate"] == "" {
		log.Fatalf("expected a cert to be generated")
	}
}
