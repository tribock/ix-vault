package vault

import (
	"github.com/hashicorp/vault/api"
)

// GetVaultClient initializes a vault client
func GetVaultClient(vaultAddress, token string) (*VaultClient, error) {

	client, err := api.NewClient(&api.Config{
		Address: vaultAddress,
	})
	client.SetToken(token)

	return &VaultClient{client}, err
}
